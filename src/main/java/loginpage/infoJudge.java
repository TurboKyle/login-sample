package loginpage;

import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

abstract class infoJudge {
    static File infoFile = new File(System.getProperty("user.dir") + "/info.YAML");
    static Yaml yaml = new Yaml();
    String seed;
    boolean isPassed;


    public static boolean verCert() {
        //TODO
        return false;
    }

    public static void rememPass(boolean status) {
        //TODO
    }

    public char[] getSeed(String username) {
        //TODO
        return new char[0];
    }

    public static HashMap checkPass(String username, String pwd) {
        Map info = null;
        HashMap rst = new HashMap<String, String>();

        try {
            info = (Map) yaml.load(new FileInputStream(infoFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            rst.put("error", "file");
        }

        assert info != null;
        if (username.equals(info.get("Username"))) {
            if (MD5Tools.encode(pwd).equals(info.get("Password"))) {
                rst.put("status", "0");
            } else {
                rst.put("status", "-1");
            }
        } else {
            rst.put("status", "1");
        }

        return rst;
    }

}
