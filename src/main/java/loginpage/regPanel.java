package loginpage;

import org.yaml.snakeyaml.Yaml;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class regPanel extends JDialog {
    JPanel rootPanel;
    private JTextField unField;
    private JPasswordField pwdField;
    private JPasswordField skField;
    private JButton returnButton;
    private JButton verifyButton;

    private String fileName = "info.YAML";

    public regPanel() {

        JFrame frame = new JFrame("regPanel");
        frame.setResizable(false);
        frame.setMinimumSize(new Dimension(300, 200));
        frame.setTitle("Register");
        frame.setContentPane(this.rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        verifyButton.addActionListener(actionEvent -> {
            String path = System.getProperty("user.dir");
            try {
                File infoFile = new File(path + "/" + fileName);
                try {
                    if (infoFile.canWrite()) {
                        Writer out = new FileWriter(infoFile);
                        Yaml yaml = new Yaml();
                        Map<String, Object> rootTable = new HashMap<>();
                        rootTable.put("Username", unField.getText());
                        rootTable.put("Password", MD5Tools.encode(pwdField.getText()));
                        yaml.dump(rootTable, out);
                        JOptionPane.showMessageDialog(null, "User" + " " + rootTable.get("Username") + " " + "register successfully.", "INFO", JOptionPane.INFORMATION_MESSAGE);
                        frame.dispose();
                    } else {
                        JOptionPane.showMessageDialog(null, "File Permission Denied", "Error", JOptionPane.ERROR_MESSAGE);
                        frame.dispose();
                    }
                } catch (IOException ioException) {
                    JOptionPane.showMessageDialog(null, ioException.toString(), "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        });

        returnButton.addActionListener(actionEvent -> {
            frame.dispose();
        });

    }
}
