package loginpage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;

public class loginPage {

    private JPanel rootPanel;
    private JTextField usernameField;
    private JPasswordField passwordField;
    private JButton loginButton;
    private JButton regButton;
    private JCheckBox rpCheckbox;
    private JCheckBox caCheckbox;

    public loginPage() {
        loginButton.addActionListener(e -> {
            String userName = usernameField.getText();
            String pwd = passwordField.getText();
            HashMap status = infoJudge.checkPass(userName, pwd);
            switch ((String) status.get("status")) {
                case "-1":
                    JOptionPane.showMessageDialog(null, "Error", "Wrong Password", JOptionPane.ERROR_MESSAGE);
                    break;
                case "0":
                    JOptionPane.showMessageDialog(null, "Passed", "Login Passed", JOptionPane.INFORMATION_MESSAGE);
                    break;
                case "1":
                    JOptionPane.showMessageDialog(null, "Error", "No such username", JOptionPane.ERROR_MESSAGE);
                    break;
            }
        });
        regButton.addActionListener(e -> {
            regPanel reg = new regPanel();
            reg.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);

        });
        rpCheckbox.addItemListener(itemEvent -> {
            //TODO
        });
        caCheckbox.addItemListener(itemEvent -> {
            //TODO
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("loginPage");
        frame.setResizable(false);
        frame.setMinimumSize(new Dimension(400, 200));
        frame.setTitle("Login");
        frame.setContentPane(new loginPage().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.pack();
        frame.setVisible(true);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
